import { useState } from 'react';
import { useCurrentRates } from '../services/getCurrentRates';
import Table from '../components/table/Table';
import { Rate } from '../_generated/typings/rates/Rate';
import styles from './HomePage.module.scss';
import SearchBar from '../components/for_pages/home_page/SearchBar';
import Author from '../components/for_pages/home_page/Author';

const tableHeaders = ['země', 'měna', 'množství', 'kód', 'kurz'];

const transformDataRows = (dataRows: Rate[]): string[][] =>
  dataRows.map(({ country, currency, amount, code, rate }) => [
    country,
    currency,
    '' + amount,
    code,
    '' + rate
  ]);

const HomePage = () => {
  const { isLoading, error, data } = useCurrentRates();
  const [currencyChosen, setCurrencyChosen] = useState<number>();

  return (
    <div className={styles.div}>
      <SearchBar
        rates={data?.dataRows}
        currencyChosen={currencyChosen}
        setCurrencyChosen={setCurrencyChosen}
      />
      {!isLoading && (error || !data) ? (
        'error occured'
      ) : (
        <Table
          headerRow={tableHeaders}
          dataRows={
            isLoading || !data
              ? []
              : transformDataRows(
                  currencyChosen
                    ? data.dataRows.filter(({ id }) => id === currencyChosen)
                    : data.dataRows
                )
          }
          isLoading={isLoading}
        />
      )}
      <Author />
    </div>
  );
};

export default HomePage;
