import React from 'react';

export interface IconProps extends React.HTMLProps<HTMLDivElement> {}
