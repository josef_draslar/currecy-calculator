import React, { FC, ReactElement, ReactNode } from 'react';
import styles from './Button.module.scss';
import classNames from 'classnames';

interface ButtonProps extends React.HTMLProps<HTMLButtonElement> {
  type?: 'button' | 'submit' | 'reset';
}

interface Props extends ButtonProps {
  missing?: string;
  isPrimary?: boolean;
  svgIcon?: ReactElement;
  children: ReactNode;
  disabled?: boolean;
  href?: string;
  className?: string;
}

const Button: FC<Props> = ({ missing, svgIcon, children, disabled, className, ...props }) => {
  const innerContent = (
    <span>
      {svgIcon}
      {children}
    </span>
  );

  const classNamesButton = classNames(styles.button, {
    [styles['has-icon']]: !!svgIcon,
    [styles.disabled]: disabled
  });

  return (
    <div className={classNames(styles.wrapper, className)}>
      {missing && <div className={styles.missing}>Please fill: {missing}</div>}
      <button className={classNamesButton} disabled={disabled} {...(props as ButtonProps)}>
        {innerContent}
      </button>
    </div>
  );
};

export default Button;
