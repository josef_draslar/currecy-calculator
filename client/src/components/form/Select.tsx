import React, { FC, useMemo } from 'react';
import classNames from 'classnames';
import { getUniqueId } from '../../utils/getUniqueId';
import styles from './Select.module.scss';

interface Props extends React.HTMLProps<HTMLSelectElement> {
  label: string;
  error?: string;
  width?: string;
  iconClass?: string;
  values: { label: string; value: string }[];
}

const Select: FC<Props> = ({ values = [], label: inputText, error, ...props }) => {
  const id = useMemo(getUniqueId, []);

  return (
    <fieldset
      className={classNames(styles['field-set'], {
        [styles['has-error']]: !!error
      })}>
      <select className={styles.input} id={id} {...props}>
        <option value={''} />
        {values.map(({ label, value }) => (
          <option key={value} value={value}>
            {label}
          </option>
        ))}
      </select>
      <label
        htmlFor={id}
        className={classNames(styles.label, {
          [styles['label-with-content']]: !!props.value
        })}>
        {inputText}
      </label>
      {!!error && <div className={styles.error}>{error}</div>}
    </fieldset>
  );
};

export default Select;
