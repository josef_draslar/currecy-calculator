import React, { FC, useMemo } from 'react';
import classNames from 'classnames';
import { getUniqueId } from '../../utils/getUniqueId';
import styles from './Input.module.scss';
import { IconAngleUp } from '../../icons/IconAngleUp';
import { IconAngleDown } from '../../icons/IconAngleDown';

interface Props extends React.HTMLProps<HTMLInputElement> {
  label: string;
  error?: string;
  width?: string;
  iconClass?: string;
  className?: string;
  justDecimalNumbers?: boolean;
  // eslint-disable-next-line no-unused-vars
  incrementDecrementCb?: (isIncrement: boolean) => void;
}

const Input: FC<Props> = ({
  label,
  error,
  onChange,
  incrementDecrementCb,
  width,
  iconClass,
  className,
  id,
  justDecimalNumbers,
  ...props
}) => {
  const uniqueId = useMemo<string>(getUniqueId, []);
  const idLocal: string = id || uniqueId;

  const additionalAttributes: React.HTMLProps<HTMLInputElement> = {};
  if (justDecimalNumbers) {
    additionalAttributes.pattern = 'd*';
    additionalAttributes.min = 1;
    additionalAttributes.max = 1000000000000;
  }

  return (
    <fieldset
      style={{ width }}
      className={classNames(styles['field-set'], { [styles['has-error']]: error }, className)}>
      <input
        id={idLocal}
        autoCorrect="off"
        spellCheck="false"
        autoCapitalize="off"
        onChange={onChange}
        className={classNames(styles.input, {
          [styles['is-empty']]: !props.value,
          [styles['has-icon']]: !!iconClass
        })}
        {...additionalAttributes}
        {...props}
      />
      {!!iconClass && <i className={classNames(styles.icon, iconClass)} />}
      <label
        htmlFor={idLocal}
        className={classNames(styles.label, {
          [styles['label-with-content']]: !!props.value
        })}>
        {label}
      </label>
      {!!incrementDecrementCb && (
        <span className={styles['angle-icons']}>
          <IconAngleUp onClick={() => incrementDecrementCb(true)} />
          <IconAngleDown onClick={() => incrementDecrementCb(false)} />
        </span>
      )}
      {!!error && <div className={styles.error}>{error}</div>}
    </fieldset>
  );
};

export default Input;
