import { FC } from 'react';
import styles from './Table.module.scss';
import classNames from 'classnames';

type Props = {
  title?: string;
  headerRow: string[];
  dataRows: string[][];
  isLoading?: boolean;
};

const Table: FC<Props> = ({ title, headerRow, dataRows, isLoading }) => {
  return (
    <section className={classNames(styles.section, styles['animated-first-load'])}>
      {!!title && <h1>{title}</h1>}
      <div className={styles.header}>
        <table cellPadding="0" cellSpacing="0">
          <thead>
            <tr>
              {headerRow.map((val) => (
                <th key={val}>{val}</th>
              ))}
            </tr>
          </thead>
        </table>
      </div>
      <div className={classNames(styles.content, { [styles['is-loading']]: !!isLoading })}>
        <table cellPadding="0" cellSpacing="0">
          <tbody>
            {!!isLoading && (
              <tr>
                {headerRow.map((_, idx) => (
                  <td key={idx}>
                    <span className={styles.loading} />
                  </td>
                ))}
              </tr>
            )}
            {dataRows.map((row, index) => (
              <tr key={index}>
                {row.map((val, idx) => (
                  <td key={idx}>{val}</td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </section>
  );
};

export default Table;
