import React, { FC, useMemo, useState } from 'react';
import Input from '../../form/Input';
import Select from '../../form/Select';
import { Rate } from '../../../_generated/typings/rates/Rate';
import Button from '../../form/Button';
import styles from './SearchBar.module.scss';
import { getRatesById, RatesById } from '../../../utils/helpers/getRatesById';
import { formatCurrencyNumber } from '../../../utils/helpers/formatCurrencyNumber';
import { getForeignCurrencyAmount } from '../../../utils/helpers/getForeignCurrencyAmount';
import { formatNumber } from '../../../utils/helpers/formatNumber';

type Props = {
  rates?: Rate[];
  currencyChosen?: number;
  // eslint-disable-next-line no-unused-vars
  setCurrencyChosen: (key: number) => void;
};

const SearchBar: FC<Props> = ({ rates = [], currencyChosen, setCurrencyChosen }) => {
  const [amount, setAmount] = useState('');
  const [calculation, setCalculation] = useState(0);

  const ratesByCurrency: RatesById = useMemo<RatesById>(() => getRatesById(rates), [rates]);

  const calculate = (amountParam: string = amount, currencyParam: number = currencyChosen || 0) => {
    setCalculation(getForeignCurrencyAmount(amountParam, currencyParam, ratesByCurrency));
  };

  const onChangeAmount = ({ target }: React.ChangeEvent<HTMLInputElement>) => {
    const newAmount = target.value.replace(/\D/g, '').substr(0, 15);
    setAmount(newAmount);
    calculate(newAmount, currencyChosen);
  };
  const onChangeCurrency = ({ target }: React.ChangeEvent<HTMLSelectElement>) => {
    const newCurrencyChosen = target.value ? parseInt(target.value, +10) : 0;
    setCurrencyChosen(newCurrencyChosen);
    calculate(amount, newCurrencyChosen);
  };

  const incrementDecrementCb = (isIncrement: boolean) => {
    const parsedAmount = parseInt(amount, 10);
    if (isIncrement) {
      setAmount(amount ? '' + (parsedAmount + 1) : '1');
    } else {
      setAmount(parsedAmount > 1 ? '' + (parsedAmount - 1) : amount);
    }
  };

  return (
    <section className={styles.section}>
      <Input
        label="Částka"
        value={formatNumber(amount)}
        onChange={onChangeAmount}
        type="text"
        justDecimalNumbers
        incrementDecrementCb={incrementDecrementCb}
      />
      <Select
        label="Měna"
        value={currencyChosen}
        values={rates.map(({ country, currency, id }) => ({
          label: `${currency} (${country})`,
          value: '' + id
        }))}
        onChange={onChangeCurrency}
      />
      <Button onClick={() => calculate()}>Přepočítat</Button>
      <h2>
        {!!currencyChosen && !!calculation && (
          <span>{formatCurrencyNumber(calculation, ratesByCurrency[currencyChosen].code)}</span>
        )}
      </h2>
    </section>
  );
};

export default SearchBar;
