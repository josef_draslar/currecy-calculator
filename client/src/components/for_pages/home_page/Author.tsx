import styles from './Author.module.scss';

const Author = () => (
  <div className={styles['made-with-love']}>
    Made with <i>♥</i> by{' '}
    <a target="_blank" href="https://www.linkedin.com/in/josef-draslar-b48222140/" rel="noreferrer">
      Josef Draslar
    </a>
  </div>
);

export default Author;
