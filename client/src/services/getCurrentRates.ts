import routes from '../_generated/routes.json';
import { useQuery } from 'react-query';
import { RateTable } from '../_generated/typings/rates/Rate';

const getCurrentRates = (): Promise<RateTable> =>
  fetch(routes['rates/current_rate']).then((res) => res.json());

export const useCurrentRates = () => useQuery<RateTable>('currentRates', getCurrentRates);
