let uniqueIdNumber = 0;

export const getUniqueId = () => `react-unique-id-${uniqueIdNumber++}`;
