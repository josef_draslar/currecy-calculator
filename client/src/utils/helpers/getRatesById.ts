import { Rate } from '../../_generated/typings/rates/Rate';

export type RatesById = {
  [key: string]: Rate;
};

export const getRatesById = (rates: Rate[]) =>
  rates.reduce((acc: RatesById, val: Rate): RatesById => {
    acc[val.id] = val;
    return acc;
  }, {});
