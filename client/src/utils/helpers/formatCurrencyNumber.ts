export const formatCurrencyNumber = (val: number, currency: string): string =>
  new Intl.NumberFormat('cs-CZ', { style: 'currency', currency, maximumFractionDigits: 3 }).format(
    val
  );
