export const formatNumber = (val: string | number | undefined): string => {
  if (!val) return '';
  const parsedVal = typeof val === 'string' ? parseInt(val, 10) : val;
  const formatter = new Intl.NumberFormat('cs-CZ', { maximumFractionDigits: 0, useGrouping: true });
  return formatter.format(parsedVal);
};
