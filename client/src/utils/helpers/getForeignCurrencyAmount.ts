import { RatesById } from './getRatesById';

export const getForeignCurrencyAmount = (
  amount: string,
  currencyId: number,
  ratesByCurrency: RatesById
) => (amount && currencyId ? parseInt(amount) / ratesByCurrency[currencyId].rateForOneCzk : 0);
