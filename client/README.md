# Steps to production

- set up pino to push to 3rd party cloud logging service
- set up 3rd party cloud error loggin service
- set title, favicon and logos

# Next steps

- add unit tests for server
- add shallow render tests for client
- add automated tests for client
- add multilanguage support
- set https
- tune react-query performance
- error state (search for 'error occured' in codebase)
- add error boundary

# Known bugs

- currency select box list is not opened upon label click, input is just highlighted
- currency select box input is still highlighted after leaving (closing) the option list

# Start development server

- `npm run start`
