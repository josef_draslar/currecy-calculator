import { axios } from '../helpers/axios';

describe('API Rates - getCurrentRates', () => {
  test('success', async () => {
    const response = await axios.get('http://localhost:3003/api/rates/current_rate');
    expect(response.status).toBe(200);
    expect(response.data).toBeDefined();
    expect(response.data.date).toBeDefined();
    expect(response.data.headers).toEqual([ 'země', 'měna', 'množství', 'kód', 'kurz' ]);
    expect(response.data.dataRows).toBeDefined();
    expect(response.data.dataRows.length).not.toBe(0);
    expect(response.data.dataRows[0].country).toBeDefined();
    expect(response.data.dataRows[0].currency).toBeDefined();
    expect(response.data.dataRows[0].amount).toBeDefined();
    expect(response.data.dataRows[0].code).toBeDefined();
    expect(response.data.dataRows[0].rate).toBeDefined();
  });
});
