import axiosLib from 'axios';
const axiosConfig = {
  baseURL: 'http://localhost:' + process.env.PORT,
  timeout: 5000,
};

export const axios = axiosLib.create(axiosConfig);
