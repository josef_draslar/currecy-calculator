# Run tests
- in one terminal run `npm run start:test`
- in other terminal run `npm run test`

# Next steps
- set (and force) https
- create swagger api documentation or restrict CORS

# Start development server
- `npm run start:dev`