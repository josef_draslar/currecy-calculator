import { getCurrentRates } from './getCurrentRates';
import { RateTable } from '../../typings/rates/Rate';

type RateServiceType = {
  getCurrentRates: ()=>Promise<RateTable>
};

export const RateService: RateServiceType = {
  getCurrentRates,
};