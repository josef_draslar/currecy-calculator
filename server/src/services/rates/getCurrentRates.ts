import axios from 'axios';
import { getCnbCurrentRateApiUrl } from '../../config/getEnvVariable';
import { ValidationError } from '../../utils/ValidationError';
import { Rate, RateTable } from '../../typings/rates/Rate';

const fetchCurrentRate = async (): Promise<string> => (await axios.get(getCnbCurrentRateApiUrl())).data;

const parseCurrentRateData = (data: string): RateTable => {
  if (!data) {
    throw new ValidationError('No data returned from CNB API', 'response data:', data);
  }
  const rows = data.split('\n');

  if (rows.length < 4) {
    throw new ValidationError('Insufficient number of rows returned from CNB API (expected at least 3)', 'response rows:', rows);
  }

  const date = rows.shift()?.split('#')[0].trim() || '';
  const headers = rows.shift()?.split('|');

  if (headers?.length !== 5) {
    throw new ValidationError('Wrong number of columns returned from CNB API (expected at least 5)', 'response headers:', headers);
  }


  const dataRows = rows.filter((row: string) => !!row).map((row: string, idx: number): Rate => {
    const [country, currency, amount, code, rate] = row.split('|');
    const amountParsed = parseInt(amount, 10);
    const rateParsed = parseFloat(rate.replace(',', '.'));
    return {
      id: idx + 1,
      country,
      currency,
      amount: amountParsed,
      code,
      rate: rateParsed,
      rateForOneCzk: rateParsed / amountParsed,
    };
  });

  return {
    date,
    headers,
    dataRows,
  };
};

export const getCurrentRates = async () => parseCurrentRateData(await fetchCurrentRate());