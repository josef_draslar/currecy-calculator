import express, { Request, Response } from 'express';
import routes from './routes.json';
import { getCurrentRates } from '../services/rates/getCurrentRates';
import { logError } from '../utils/logError';
import { RateTable } from '../typings/rates/Rate';

const router = express.Router();

router.get(routes['rates/current_rate'], async (req: Request, res: Response): Promise<void> => {
  try {
    const data: RateTable = await getCurrentRates();
    res.send(data);
  } catch (e) {
    logError(e);
    res.status(400).end();
  }
},
);

export default router;