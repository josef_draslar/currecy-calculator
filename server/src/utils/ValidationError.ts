export class ValidationError extends Error {
  constructor(message: string, dataLabel?: string, data?: any) {
    super(dataLabel ? `${message}. ${dataLabel}: ${JSON.stringify(data)}` : message);
    this.name = 'ValidationError';
  }
}