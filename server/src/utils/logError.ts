import { logger } from '../config/logger';

export const log = (message: string) => {
  logger.logger.info({ message });
};

export const warn = (message: string) => {
  logger.logger.warn({ message });
};

export const logError = (e: any) => {
  logger.logger.error(e);
};