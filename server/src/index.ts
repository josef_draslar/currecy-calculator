import cors from 'cors';
import express from 'express';
import RateRouter from './routes/RateRouter';
import dotenv from 'dotenv';
import path from 'path';
import { logger } from './config/logger';
import { errorHandler } from './config/errorHandler';
dotenv.config();

if (!process.env.CNB_GET_CURRENT_RATES_URL) {
  throw new Error('env CNB_GET_CURRENT_RATES_URL has to be set');
}
const PORT = process.env.PORT || 3003;

const app = express();

app.use(errorHandler);
app.use(logger);
app.use(cors());
app.use(express.json());
app.use(RateRouter);


if (process.env.NODE_ENV === 'production') {
  app.use(express.static(path.resolve(__dirname, '../../client/build')));
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../client/build', 'index.html'));
  });
}

app.listen(PORT, () => {
  // tslint:disable-next-line:no-console
  console.log(`Example app listening on port ${PORT}`);
});