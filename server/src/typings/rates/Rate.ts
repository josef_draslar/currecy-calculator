export type Rate = { id: number, country: string, currency: string, amount: number, code: string, rate: number, rateForOneCzk: number };

export type RateTable = {
  date: string,
  headers: string[],
  dataRows: Rate[]
};