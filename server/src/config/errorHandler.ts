import { Request, Response } from 'express';
import { logError } from '../utils/logError';

export const errorHandler = (err: Error, req: Request, res: Response, next: any) => {
  if (res.headersSent) {
    return next(err);
  }
  logError(err);
  res.status(500).end();
};