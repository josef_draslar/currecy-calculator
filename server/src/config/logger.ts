import pino from 'pino-http';

export const logger = pino({
  quietReqLogger: true,
  transport: {
    target: 'pino-http-print',
    options: {
      all: true,
      colorize: true,
      translateTime: true,
    },
  },
  customAttributeKeys: {
    req: 'request',
    res: 'response',
    err: 'error',
    responseTime: 'timeTaken',
  },
  serializers: {
    req: ({ method, url, query, params }) => ({
      method,
      url,
      query,
      params,
    }),
    res: ({ statusCode }) => ({
      statusCode,
    }),
    err: ({ message, stack, config }) => ({
      message,
      stack,
      config: config ? { url: config.url, method: config.method } : undefined,
    }),
  },
});