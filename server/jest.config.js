require('dotenv').config()

module.exports = {
  setupFilesAfterEnv: ['./test/jest.setup.js'],
  preset: 'ts-jest',
  testEnvironment: 'node',
  "roots": [
      "test"
  ],
  "testMatch": [
    "**/*.test.js"
  ],
  "transform": {
    "^.+\\.(ts|tsx)$": "ts-jest"
  },
  globals: {
    'ts-jest': {
      diagnostics: {
        "warnOnly": true
      }
    },
  },
};