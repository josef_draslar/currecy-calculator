# Currency calculator
- [running instance](https://currency-calculator-josef.herokuapp.com/)

# Currency calculator
- see [client-readme](client/README.md) and [server-readme](server/README.md)

# Deploy
- `heroku login`
- (set remote heroku origin)
- `heroku push heroku master`